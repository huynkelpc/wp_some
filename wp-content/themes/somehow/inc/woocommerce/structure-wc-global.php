<?php
// Remove Default links
remove_action( 'woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open', 10);
add_action( 'somehow_shop_loop_item_title','woocommerce_template_loop_product_link_open', 10);

// Remove Default links
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail', 10);
add_action( 'somehow_template_loop_product_thumbnail','woocommerce_template_loop_product_thumbnail', 10);

// Remove Price Category
remove_action( 'woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price', 10);
add_action( 'somehow_after_shop_loop_item_title','woocommerce_template_loop_price', 10);

// Remove Product Sale
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);

// Remove Order Product
remove_action( 'woocommerce_before_shop_loop','woocommerce_catalog_ordering', 10);
add_action( 'somehow_woocommerce_catalog_ordering','woocommerce_catalog_ordering', 10);
// Remove Order Product
remove_action( 'woocommerce_before_shop_loop','woocommerce_result_count', 10);
add_action( 'somehow_woocommerce_result_count','woocommerce_result_count', 10);
?>
