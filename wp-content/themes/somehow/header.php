<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<?php
wp_body_open();
?>
<header>
    <div class="header-top clearfix hidden-xs hidden-sm">
        <div class="container">

            <h1 class="logo">
                <a href="index.html" class="brand">
                    <?php somehow_site_logo(); ?>
                </a>
            </h1>
            <div class="user-cart hidden-xs hidden-sm">
                <a href="account/login">Đăng nhập</a>
                <a href="cart.html" class="cart-top">
                    Giỏ hàng
                    <?php woocommerce_mini_cart() ?>
                </a>
            </div>
        </div>
    </div>
    <div class="header-bottom clearfix hidden-xs hidden-sm">
        <div class="container">

            <div class="menu-center">
                <ul class="main-menu clearfix">

                    <li>
                        <a class="active" href="index.html">Trang chủ</a>
                    </li>

                    <li class="">
                        <a class="" href="collections/all.html">Sản phẩm</a>

                        <div class="sub">
                            <div class="sub-container">
                                <ul class="menu-item bg">

                                    <li><a href="collections/san-pham-moi.html">SẢN PHẨM MỚI</a></li>

                                    <li><a href="collections/san-pham-ban-chay.html">SẢN PHẨM BÁN CHẠY</a></li>

                                    <li><a href="collections/san-pham-khuyen-mai.html">SALE OFF: UP TO 50%</a></li>

                                </ul>
                                <ul class="menu-item">

                                    <li><a href="collections/ao-khoac.html">ÁO KHOÁC</a>

                                    </li>

                                    <li><a href="collections/ao-so-mi.html">ÁO SƠ MI</a>

                                    </li>

                                    <li><a href="collections/ao-thun.html">ÁO THUN</a>

                                    </li>

                                    <li><a href="collections/ao-polo.html">ÁO POLO</a>

                                    </li>

                                    <li><a href="collections/ao-ni.html">ÁO NỈ</a>

                                    </li>

                                    <li><a href="collections/ao-len.html">ÁO LEN</a>

                                    </li>

                                    <li><a href="collections/hoodies.html">HOODIES</a>

                                    </li>

                                </ul>
                                <ul class="menu-item">

                                    <li><a href="collections/quan-jean.html">QUẦN JEAN</a>

                                    </li>

                                    <li><a href="collections/quan-kaki.html">QUẦN KAKI</a>

                                    </li>

                                    <li><a href="collections/quan-tay.html">QUẦN TÂY</a>

                                    </li>

                                    <li><a href="collections/quan-dui.html">QUẦN ĐÙI</a>

                                    </li>

                                    <li><a href="collections/jogger.html">JOGGERS</a>

                                    </li>

                                    <li><a href="collections/set-quan-ao.html">SET QUẦN ÁO</a>

                                    </li>

                                </ul>
                                <ul class="menu-item">

                                    <li><a href="collections/non-vo-underwear-phu-kien.html">NÓN - VỚ - UNDERWEAR - BALO
                                            - TÚI XÁCH - PHỤ KIỆN KHÁC</a>

                                    </li>

                                    <li><a href="collections/giay-dep.html">GIÀY - DÉP</a>

                                    </li>

                                </ul>
                            </div>
                        </div>

                    </li>

                    <li>
                        <a class="" href="pages/about-us.html">Giới thiệu</a>
                    </li>

                    <li class=" has-child">
                        <a class="" href="pages/huong-dan-mua-hang-online.html">Hướng dẫn</a>

                        <ul class="child">

                            <li><a href="pages/huong-dan-mua-hang-online.html">Hướng Dẫn Mua Hàng Online</a></li>

                            <li><a href="blogs/huong-dan-lua-size.html">Hướng Dẫn Lựa Chọn Size</a></li>

                        </ul>

                    </li>

                    <li>
                        <a class="" href="pages/new-store.html">Địa chỉ cửa hàng</a>
                    </li>

                </ul>
            </div>

            <div class="search-pc pull-right"><span>Tìm kiếm</span>
                <div class="search-bar">
                    <div class="container">
                        <form action="https://somehow.vn/search" class="search-top">
                            <label>Tìm kiếm:</label>
                            <input type="hidden" name="type" value="product"/>
                            <input type="text" name="q" placeholder="Nhập từ khóa tìm kiếm..."/>
                            <button type="submit" class="search-btn">Nhập</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="header-new visible-xs visible-sm">
        <div class="container clearfix ">
            <div class="header-container clearfix">
                <div class="clearfix">
                    <div class="js-toggle-left-slidebar js-menu-mobile">
                        <div class="trigger-menu">
                            <span class="three-bars-icon"></span><span class="text">menu</span>
                        </div>
                    </div>
                    <div class="logo">
                        <a href="index.html">
                            <img alt="SomeHow Store"
                                 src="http://theme.hstatic.net/1000026602/1000473196/14/logo.svg?v=291">
                        </a>
                    </div>

                    <div class="login-icon">
                        <a href="account/login"><i class="fa fa-user" aria-hidden="true"></i></a>
                    </div>
                    <div class="menu-top clearfix">
                        <div class="search">
                            <span class="ic-search" onclick="Search();"></span>
                        </div>
                    </div>
                    <div class="search-on-mobile">
                        <div class="inside">
                            <form action="https://somehow.vn/search" method="get">
                                <input type="hidden" value="product" name="type"/>
                                <input value="" name="q" id="input-search-mobile" type="text" placeholder="Tìm kiếm...">
                                <a href="#!" class="close-search-mobile">Đóng search</a>
                            </form>
                        </div>
                    </div>
                    <div class="cart-top">
                        <a href="cart.html">
                            <img src="http://theme.hstatic.net/1000026602/1000473196/14/cart.svg?v=291"
                                 alt="cart mobile" width="16"/>
                            <span class="number-count">0</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>

<?php
// Output the menu modal.
//get_template_part('template-parts/modal-menu');
