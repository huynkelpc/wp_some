<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
				<footer id="colophon" class="site-footer">
                    <div class="footer-block">
                        <div class="container">
                            <div class="footer-address clearfix col-md-8 col-xs-12">
                                <h2 class="city">TP. Hồ Chí Minh</h2>
                                <div>

                                    <div class="adress-item">
                                        <h4>Store 1</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>17 Bis Huỳnh Đình Hai P.14 Quận Bình Thạnh</li>
                                            <li class="hotline"><a href="tel:02866506201"><i class="fa fa-phone"></i>02866 506 201</a></li>
                                        </ul>
                                    </div>

                                    <div class="adress-item" style="border-right: 1px solid white;">
                                        <h4>Store 2</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>332 Cao Đạt P.1 Quận 5</li>
                                            <li class="hotline"><a href="tel:02866506202"><i class="fa fa-phone"></i>02866 506 202</a></li>
                                        </ul>
                                    </div>

                                    <div class="adress-item">
                                        <h4>Store 3</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>188/16 Thành Thái P.12 Quận 10</li>
                                            <li class="hotline"><a href="tel:02866506203"><i class="fa fa-phone"></i>02866 506 203</a></li>
                                        </ul>
                                    </div>

                                    <div class="adress-item" style="border-right: 1px solid white;">
                                        <h4>Store 4</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>1002 Quang Trung, Phường 8, Quận Gò Vấp</li>
                                            <li class="hotline"><a href="tel:02866506204"><i class="fa fa-phone"></i>02866 506 204</a></li>
                                        </ul>
                                    </div>

                                    <div class="adress-item">
                                        <h4>Store 5</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>164-166 Trường Chinh, Phường 13, Quận Tân Bình</li>
                                            <li class="hotline"><a href="tel:02866506205"><i class="fa fa-phone"></i>028 66506205</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div class="footer-address clearfix col-md-4 col-xs-12">
                                <h2 class="city">Hà Nội</h2>

                                <div class="adress-item">
                                    <h4>Store 6</h4>
                                    <ul>
                                        <li class="add"><i class="fa fa-home"></i>79 Nguyễn Khang, Trung Hòa, Cầu Giấy</li>
                                        <li class="hotline"><a href="tel:02466562205"><i class="fa fa-phone"></i>02466 562 205</a></li>
                                    </ul>
                                </div>

                                <div class="newstore clearfix">
                                    <h2 class="city">Biên Hòa</h2>
                                    <div class="adress-item">
                                        <h4>STORE 7</h4>
                                        <ul>
                                            <li class="add"><i class="fa fa-home"></i>237, Trương Định, Biên Hòa</li>
                                            <li class="hotline"><a href="tel:0961789207"><i class="fa fa-phone"></i>0961 789 207</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-socials text-center">
                        <div class="container">
                            <h4>Like Somehow trên mạng xã hội</h4>
                            <ul class="clearfix">

                                <li><a href="https://www.facebook.com/somehow.vn" class="fb"><i class="fa fa-facebook"></i></a></li>

                                <li><a href="https://www.instagram.com/somehow.vn/" class="in"><i class="fa fa-instagram"></i></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="footer-newsletter text-center">
                        <div class="container">
                            <h4>Sign up for the newsletter</h4>
                            <p class="sub">Sign up to receive must-have style news and updates before anyone else.</p>
                            <div class="link">
                                <a href="pages/about-us.html">Contact us</a>
                            </div>
                        </div>
                    </div>

                    <div class="copyright">
                        © 2017 SOMEHOW. Văn phòng: 188/16 Thành Thái P.12, Quận 10 - TP.HCM
                    </div>
                    <a href="#" class="scrollToTop">
                        <i class="fa fa-angle-up"></i>
                    </a>
            		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
            		<div class="site-info">
            			<?php $blog_info = get_bloginfo( 'name' ); ?>
            			<?php if ( ! empty( $blog_info ) ) : ?>
            				<a class="site-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>,
            			<?php endif; ?>
            			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'somehow' ) ); ?>" class="imprint">
            				<?php
            				/* translators: %s: WordPress. */
            				printf( __( 'Proudly powered by %s.', 'somehow' ), 'WordPress' );
            				?>
            			</a>
            			<?php
            			if ( function_exists( 'the_privacy_policy_link' ) ) {
            				the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
            			}
            			?>
            			<?php if ( has_nav_menu( 'footer' ) ) : ?>
            				<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', 'somehow' ); ?>">
            					<?php
            					wp_nav_menu(
            						array(
            							'theme_location' => 'footer',
            							'menu_class'     => 'footer-menu',
            							'depth'          => 1,
            						)
            					);
            					?>
            				</nav><!-- .footer-navigation -->
            			<?php endif; ?>
            		</div><!-- .site-info -->
            	</footer><!-- #colophon -->

		<?php wp_footer(); ?>

	</body>
</html>
